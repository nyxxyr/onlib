module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        dark: '#2B2D42',
        grey: {
          '100': '#EDF2F4',
          '200': '#8D99AE'
        },
        myRed: {
          '100': '#EF233C',
          '200': '#D90429'
        }  
      },
      fontFamily: {
        'pops': ["Poppins"]
      },
      backgroundImage: theme => ({
        'booklist': "url('assets/booklist.png')",
        'newcomer': "url('assets/newcomer.jpg')",
        'about': "url('assets/about.png')"
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
