const { default: LandingPage } = require("./pages/LandingPages");

function App() {
  return (
    <div className="bg-dark min-h-screen font-pops">
      <LandingPage />
    </div>
  );
}

export default App;
