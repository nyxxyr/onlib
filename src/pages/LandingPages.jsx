import React from 'react'
import Menu from '../components/Menu'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'

const LandingPage = () => {
    const mainmenu = [
        {id: 1, name: 'Buku Baru', image: 'bg-newcomer'},
        {id: 2, name: 'Daftar Buku', image: 'bg-booklist'},
        {id: 3, name: 'Tentang Kami', image: 'bg-about'},
    ]

    return(
        <div>
            <Navbar />
            <div className="flex justify-between px-24 py-16">
                {
                    mainmenu.map((menu) => {
                        return(
                                <Menu key={menu.id} detail={menu} />
                        )
                    })
                }
            </div>
            <Footer />
        </div>
    )
}

export default LandingPage;