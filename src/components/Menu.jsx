import React from 'react' 

const Menu = (props) => {
    const detail = props.detail
    const image = "flex flex-col-reverse rounded-lg ".concat(detail.image)
    return(
        <div style={{width: '360px', height: '480px'}} className="shadow-2xl">
            <div style={{width: '360px', height: '480px'}} className={image}>
                <h4 className="text-4xl pl-5 py-6 text-grey-100 bg-dark bg-opacity-70 rounded-lg rounded-t-none">{detail.name}</h4>
            </div>
        </div>
    )
}

export default Menu;