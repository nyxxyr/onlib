import React from 'react'

const Navbar = () => {
    return(
        <div className="flex justify-between px-12 py-8 text-grey-100 text-2xl">
            <h4 className="font-bold">Onlib</h4>
            <div className="flex items-center">
                <h6>About</h6>
                <a href="#" className="ml-7 bg-grey-200 px-5 py-1.5 rounded shadow-lg">Masuk</a>
            </div>
        </div>
    )
}

export default Navbar;