import React from 'react'

const Footer = () => {
    return(
        <div className="text-center pb-4 text-grey-100">Copyright 2020 Ukmi Al-Khuwarizmi</div>
    )
}

export default Footer